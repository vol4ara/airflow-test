import logging
import os
import re
from airflow.utils.trigger_rule import TriggerRule
from airflow import DAG
from airflow.operators.python import PythonOperator, BranchPythonOperator
from datetime import datetime
import calendar
import time


def find_latest():
    sorted_files = sorted(os.listdir(), key=lambda x: re.findall(r'\d{5,}', x))
    if not sorted_files:
        return "None"
    return sorted_files[-1]


def file_creation(env_type):
    gmt = time.gmtime()
    ts = calendar.timegm(gmt)
    with open(f'civalue_{env_type}_{ts}.txt', 'w+') as f:
        f.write(f"hello ciValue from {env_type} branch")


def enviroment_branch(ti, **context):
    enviroment_type = context['dag_run'].conf['enviroment_type']
    if enviroment_type == 'production':
        return 'file_creation_production'
    elif enviroment_type == 'development':
        return 'file_creation_development'
    else:
        return ''


def file_creation_development():
    file_creation("development")


def file_creation_production():
    file_creation("production")


def print_console():
    latest = find_latest()
    logging.info(f'{latest}')
    return latest


args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime.today(),
    'email': ['lupusmodo@gmail.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'schedule_interval': None,
    'provide_context': True
}

dag = DAG(
    dag_id='test-dag',
    default_args=args,
    tags=["airflow"]

)

t1 = BranchPythonOperator(task_id="enviroment_branch",
                          python_callable=enviroment_branch,
                          dag=dag)

file_creation_development = PythonOperator(task_id="file_creation_development",
                                           python_callable=file_creation_development,
                                           dag=dag)

file_creation_production = PythonOperator(task_id="file_creation_production",
                                          python_callable=file_creation_production,
                                          dag=dag)

t4 = PythonOperator(task_id="print_console",
                    python_callable=print_console,
                    trigger_rule=TriggerRule.NONE_FAILED_MIN_ONE_SUCCESS,
                    dag=dag)

t1 >> [file_creation_development, file_creation_production] >> t4
